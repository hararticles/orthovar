# On Orthogonal Projections for Dimension Reduction and Applications in Augmented Target Loss Functions for Learning Problems

## Demonstration of the experiment "Section 7 - Application To Musical Data" for reproducibility

Article: "On Orthogonal Projections for Dimension Reduction and Applications in Augmented Target Loss Functions for Learning Problems" <br> 
Authors: A. Breger, J.I.Orlando, P. Harar, M. Dörfler, S. Klimscha, C. Grechenig, B.S. Gerendas, U. Schmidt-Erfurth, M. Ehler <br> 
Accepted in: Journal of Mathematical Imaging and Vision https://link.springer.com/journal/10851 <br>
DOI: https://doi.org/10.1007/s10851-019-00902-2 <br>
arXiv: http://arxiv.org/abs/1901.07598 <br><br>
Citation of the article (Bibtex): <br>
```
@article{breger2019on,
  title={On orthogonal projections for dimension reduction and applications in augmented target loss functions for learning problems},
  author={Breger, Anna and Orlando, Jose Ignacio and Harar, Pavol and D{\"o}rfler, Monika and Klimscha, Sophie and Grechenig, Christoph and Gerendas, Bianca S. and Schmidt-Erfurth, Ursula and Ehler, Martin},
  journal={Journal of Mathematical Imaging and Vision},
  year={2019},
  doi={10.1007/s10851-019-00902-2},
  eprinttype = {arxiv},
  eprint={1901.07598}}  
```

Citation of this repository (Bibtex):
```
@misc{harar2019ortho,
  author = {Harar, Pavol},
  title = {Orthovar: repository for reproducibility},
  year = {2019},
  howpublished = {\url{https://gitlab.com/hararticles/orthovar}}}
```

## Installation
If you want to train the network, you need to have a GPU and SSD available.

* ```sudo apt install sox```
* ```virtualenv .venv```
* ```source .venv/bin/activate```
* ```pip install -r requirements.txt``` 
* ```jupyter notebook```
* open ```orthovar.ipynb``` notebook 

## License
This project is licensed under the terms of the MIT license. See license.txt for details.

## Acknowledgement
This work was supported by International Mobility of Researchers (CZ.02.2.69/0.0/0.0/16027/0008371) and by project LO1401. The infrastructure of the SIX Center was used.

![opvvv](/uploads/a498aab910fd93068292c980cbebc414/opvvv.jpg)
