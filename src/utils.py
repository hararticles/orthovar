import time
import numpy as np
import pandas as pd
import soundfile as sf
from itertools import repeat
from keras.utils import Sequence
import os, io, copy, json, inspect
from subprocess import check_output
from matplotlib import pyplot as plt
from librosa.feature import melspectrogram
from keras.callbacks import ModelCheckpoint, Callback, CSVLogger, EarlyStopping

########################
# PATH MANIPULATIONS
########################

def ensure_folder(path):
    """Creates nested folders if they do not exist."""
    if not os.path.exists(path):
        try:
            os.makedirs(path)
        except:
            pass
         
def get_folder_fn_ext(path_file):
    """
    Splits full path 'folder1/folder2/filename.ext' to 'folder1/folder2', 'filename', 'ext'
    """
    path_folder, filename = os.path.split(path_file)
    file, ext = os.path.splitext(filename)
    return path_folder, file, ext
   

########################
# RAW WAVE MANIPULATIONS
########################
   
def remove_silence(filepath, 
                   output_precision=16, 
                   output_rate=48000, 
                   output_encoding='signed-integer', 
                   output_channels=1, 
                   above_period=1, 
                   first_shortest_signal=0.3, 
                   removal_threshold_percent=0.5, 
                   bellow_period=-1, 
                   shorten_to_dur=0.1, 
                   shorten_threshold_percent=0.5):
    
    """Uses Sox to open the wav file on filepath and returns new signal with removed silent parts."""

    assert output_precision in [8, 16, 32], 'Set to 8, 16 or 32 bits.'
    assert output_encoding in ['signed-integer', 'floating-point'], 'Set to signed-integer or floating-point.'
    dtype = 'int{}'.format(output_precision) if output_encoding == 'signed-integer' else 'float32'
    
    # EXAMPLE OF THE COMMANDS:
    # The command for silence:
    # sox input.wav output.wav silence -l 1 0.3 0.5% -1 0.1 0.5% 
    # Full command with output parameters:
    # sox input.wav -t 'wav' -b 16 -r 48000 -e signed-integer -c 1 - silence -l 1 0.3 0.5% -1 0.1 0.5%
    
    riffbytes = check_output(['sox', 
                              filepath, 
                              # OUTPUT PARAMETERS
                              '-t', 'wav', 
                              '-b', str(output_precision), 
                              '-r', str(output_rate), 
                              '-e', output_encoding, 
                              '-c', str(output_channels), 
                              '-', # PIPE output
                              'silence', 
                              '-l', 
                              '{}'.format(above_period),
                              '{}'.format(first_shortest_signal),
                              '{}%'.format(removal_threshold_percent),
                              '{}'.format(bellow_period),
                              '{}'.format(shorten_to_dur),
                              '{}%'.format(shorten_threshold_percent),
                             ])
    signal, rate = sf.read(io.BytesIO(riffbytes), dtype=dtype)
    return signal, rate, dtype

def split_to_uniform_len(signal, sample_len, stride):
    """
    Loops over signal with stride and yields segments of desired sample_len.
    Discards last segment if it is shorter than desired sample_len.
    """
    
    assert stride > 0, 'Stride must be a positive integer. Now is {}'.format(stride)
    assert sample_len > 0, 'Sample_len must be a positive interger. Now is {}'.format(sample_len)
    
    starts = np.arange(len(signal))[::stride]
    ends = (starts + sample_len).astype(np.int)
    for start, end in zip(starts, ends):
        sample = signal[start:end]
        if len(sample) == sample_len:
            yield sample
            

def apply_window(signal):
    """Applies Tukey window to signal."""
    from scipy.signal.windows import tukey
    return signal * tukey(len(signal), alpha=0.25)


def prepare_segments(row_tuple, paths, params):
    """
    Opens the wavfile from path that is found in pandas series on specified column name.
    Removes silence according to default setting of the silence removal function.
    Splits the file into segments of the same length. Saves them in specified folder.
    
    Params:
    --------
    row_tuple: tuple of int and pandas.Series
        Output of pandas iterrows(). Series containing the info about original wav file.
    paths: dict
        Containing the following keys:
            path_original: str
                Path to parent folder of sound_files of original GoodSounds dataset
            path_preprocessed: str
                Path to parent folder where the segments will be saved in original structure
    params: dict
        Config containing 'sample_dur_s' and 'stride_perc' keys
        
    Returns:
    --------
        pd.DataFrame containg information about the segments
    """
    
    # Prepare variables
    row = row_tuple[1]
    
    # Get paths
    path_src_wav = os.path.join(paths['path_original'], row['filename'])
    path_folder, fn, ext = get_folder_fn_ext(row['filename'])
    
    # Remove silence
    signal, rate, dtype = remove_silence(path_src_wav, output_rate=params['rate'])
    
    # Split to segments
    rowlist = []
    sample_len = params['sample_dur_s'] * rate
    
    # Set stride
    if isinstance(params['stride_perc'], dict):
        stride = max(1, int(params['stride_perc'][row['instrument']] / 100 * sample_len))
    else:   
        stride = max(1, int(params['stride_perc'] / 100 * sample_len))

    for i, sample in enumerate(split_to_uniform_len(signal, sample_len, stride)):
        r = copy.copy(row)
        
        # Compute metadata
        segment_id = '{:05d}'.format(i)
        segment_filename = '{}_{}{}'.format(fn, segment_id, ext)
        segment_folder = os.path.join(paths['path_preprocessed'], path_folder)
        r['segment_id'] = segment_id
        r['segment_path'] = os.path.join(segment_folder, segment_filename)
        
        if os.path.exists(r['segment_path']) and not params['overwrite']:
            rowlist.append(r) # Update csv
            continue           
        
        # Apply window transform to the data if necessary
        if 'window' in params.keys() and params['window']:
            sample = apply_window(sample).astype(dtype)
        
        # Save signal
        ensure_folder(segment_folder)
        sf.write(r['segment_path'], sample, rate)
        
        rowlist.append(r) # Update csv
    
    # Create pandas df
    df_segments = pd.DataFrame()
    if rowlist:
        df_segments = df_segments.append(rowlist, ignore_index=True)
        df_segments = df_segments[['take_id', 'sound_id', 'instrument', 'segment_id', 'segment_path']]
    
    return df_segments


########################
# SPLITTING
########################

def enough_segments(df, groupby_col, segments_col, return_col, n, mode='random', seed=10):
    """
    Groups the df and picks some rows so their segments_col sum up to >= n.
    Mode can be: 
        'smallest' = picks rows with smallest values in segments_col first
        'largest' = the opposit of 'smallest'
        'random' = picks rows at random (downside is that it can take a bit more rows than necessary)
        
    segments_col is a name of column based on which we compute our n
    """
    if n == 0:
        return []
    groups = []
    for (_, group) in df.groupby(groupby_col):
        
        if mode == 'smallest':
            group = group.sort_values(by=segments_col, ascending=True)
        elif mode == 'largest':
            group = group.sort_values(by=segments_col, ascending=False)
        else:
            group = group.sample(frac=1, random_state=seed) # Shuffle
        
        up_to_idx = np.argmax(np.cumsum(group[segments_col].values) >= n)
        groups.append(group.iloc[0:up_to_idx + 1])
    return pd.concat(groups)[return_col].values


########################
# TRANSFORM TO TIME-FREQUENCY REPRESENTATION AND SCALE
########################

# Transform function
def to_mel(signal, kwargs):
    return melspectrogram(signal, **kwargs)

 
# Scaling functions
def get_scaling_f(mode):
    if mode == None:
        def identity(tensor):
            """No scaling."""
            return tensor
        return identity
    
    elif mode == 'log':
        def scale_log(tensor):
            """Natural log scaling."""
            return np.log(tensor + 1e-07)
        return scale_log
    else: 
        raise NotImplemented()
        
        
########################
# PLOTTING
########################


def plot_window(nsamples=24000):

    # Print the tukey window
    temp = np.ones((nsamples,))
    tuk = apply_window(temp)

    plt.plot(tuk)
    plt.xlim(0, nsamples)
    plt.ylim(0)
    plt.title('Tukey window, alpha=0.25')
    plt.xlabel('samples')
    plt.ylabel('amplitude')
    plt.xticks([0, nsamples//4, nsamples//2, 3*(nsamples//4) ,nsamples])
    plt.show()
        
        
def plot_one_spectrogram(sequence, mel_params, onehot_to_label,
                         out_file=None, batch_idx=0, 
                         sample_idx=0, figsize=(15,5),
                         trained_redistributor=None, 
                         print_stats=False, fontsize=12):
    
    from librosa.display import specshow
    from sklearn.preprocessing import MinMaxScaler
    
    # Get the batch from sequence and measure time
    st = time.time()
    x, y = sequence.__getitem__(batch_idx)
    et = time.time()

    if trained_redistributor:
        # Inverse transform of the redistributed sample
        sample = trained_redistributor.inverse_transform(np.array([x[sample_idx]]))[0]
    else:
        # One sample from batch scaled to interval [0, 1]
        x_scaled = MinMaxScaler().fit_transform((x).reshape(-1,1)).reshape(x.shape)
        sample = x_scaled[sample_idx]

    # Plot maptlotlib image
    if figsize:
        plt.figure(figsize=figsize)
    specshow(sample, y_axis='mel', x_axis='time', 
             sr=mel_params['sr'], hop_length=mel_params['hop_length'], vmin=0, vmax=1, rasterized=False)
    
    plt.xlabel('Time [s]', fontsize=fontsize, labelpad=40)
    plt.ylabel('Frequency [Hz]', fontsize=fontsize, labelpad=40)
    plt.tick_params(axis='both', which='major', labelsize=fontsize)
    plt.title('Instrument: {}'.format(onehot_to_label[tuple(y[sample_idx])]))
    plt.xticks([0.25, 0.5])
    
    # Colorbar
    cbar = plt.colorbar()
    cbar.ax.tick_params(labelsize=fontsize)
    
    plt.tight_layout()
    if out_file:
        plt.savefig(out_file)

    if print_stats:
        print('Time to load batch: {:.5f} s'.format(et - st))
        print('Batch X shape: {}, Batch Y shape: {}'.format(x.shape, y.shape))
        print('Label: ', y[sample_idx])
        print('Sample min: {}, Sample max {}'.format(np.min(sample[sample_idx]), np.max(sample[sample_idx])))
    
########################
# TRAINING
########################

class EvaluateOnSequence(Callback):

    """
    This callback takes keras.utils.Sequence as input and evaluates all metrics
    that model uses on the whole Sequence updating the logs of the epoch.
    User can specify name for the sequence which appears in the name of the
    key in the log dictionary. Eg. seq_loss or seq_acc.

    Useful when you want to evaluate the performance of the model on the whole
    training set after each epoch instead of the average that you get during trainig.
    Also useful for evaluating on a second validation set.
    """

    def __init__(self, sequence, name='seq'):
        super(EvaluateOnSequence, self).__init__()
        self.sequence = sequence
        self.name = name

    def on_epoch_end(self, epoch, logs=None):
        results = self.model.evaluate_generator(self.sequence, max_queue_size=10, workers=8, use_multiprocessing=True)
        for metric, result in zip(self.model.metrics_names, results):
            logs['{}_{}'.format(self.name, metric)]  = result
            
            
def custom_loss(features, project_n=None, alpha=0.1, alpha_trainable=False):

    """
    features: np.array
        One row of features per base class.
        First row of features corresponds to first class, e.g. [1,0,0,0,0,0] etc.
        
    project_n: int or None
        If int, features will be orthogonaly projected to int dim space.
        If None, features will be kept untouched.
        
    alpha: float
        Weight of the second loss function term (features of output)
        If alpha=0, the second loss does not have any effect
    """
    import keras.backend as K
    
    features = K.constant(features)
    
    def project(features_vector, random_matrix):
        # Random orthogonal projections to n-dim space
        P, _ = np.linalg.qr(random_matrix)
        return K.transpose(K.dot(K.transpose(K.constant(P)), K.transpose(features_vector)))
    
    def var_loss(y_true, y_pred):
        from keras.losses import categorical_crossentropy
        l1 = categorical_crossentropy(y_true, y_pred)
        
        if alpha == 0:
            return l1
            
        true_features = K.dot(y_true, features)
        pred_features = K.dot(y_pred, features)
        
        if project_n:
            assert features.shape[-1] >= project_n, "Can't project to n>{}.".format(features.shape[-1])
            random_matrix = np.random.rand(features.shape[-1], project_n)
            true_features = project(true_features, random_matrix)
            pred_features = project(pred_features, random_matrix)
        
        from keras.losses import mean_squared_error
        l2 = mean_squared_error(true_features, pred_features)
        
        return l1 + alpha * l2
    
    return var_loss


# Generator class with tf-transformation, caching, batching, shuffling
class TimeFreqSequence(Sequence):
    """
    Creates batches of size batch_size containing data according to rep.
    Each batch is cached for faster reuse in cache_folder. Y is not saved
    in the file but mapped from labels_df DataFrame.
    """

    def __init__(self, df, labels_df, class_col, rep, batch_size, 
                 scaling_func, tf_params, cache_folder, set_name, keep_in_ram=False):
        
        self.set_name = set_name
        self.cache = os.path.join(cache_folder, rep, self.set_name, str(batch_size)) # Transformed data will be saved here
        ensure_folder(self.cache)
        self.cached_batches = [x for x in os.listdir(self.cache) if x.endswith('.npz')]
        self.rep = rep # Identifier of the transformation to perform
        self.batch_size = batch_size
        
        self.keep_in_ram = keep_in_ram
        self.ramcache = dict()
        self.class_col = class_col
        
        batch_csv = os.path.join(self.cache, 'batches.csv')
        if os.path.exists(batch_csv):
            print('File "batches.csv" exists for {}, using it instead of provided DataFrame.'.format(set_name))
            # Just load metadata if some split to batches is done already
            self.df = pd.read_csv(batch_csv)
            self.total_samples = len(self.df)
        else:
            set_df = df.loc[df['set'] == set_name].copy()
            self.total_samples = len(set_df)
            self.df = self._compute_batch_ids(set_df, self.batch_size, self.class_col)
            self.df.to_csv(batch_csv, index=False)
            
        self.labels = labels_df # Map from class name to prepared labels
        self.tf_params = tf_params
        
        self.scaling_func = scaling_func
        
    def __len__(self):
        return int(np.ceil(self.total_samples / float(self.batch_size)))
    
    
    def _transform(self, signal, rep, tf_params):
        if rep == 'raw':
            return signal
        elif rep == 'mel':
            return to_mel(signal, tf_params['mel'])
        elif rep == 'stft':
            return to_stft(signal, tf_params['stft'])
        elif rep == 'gaborscat':
            return to_gaborscat(signal, tf_params['gaborscat'])
        elif rep == 'phasederiv':
            return to_phasederiv(signal, tf_params['phasederiv'])
        else:
            raise NotImplemented("Choose one of ['mel', 'stft', 'gaborscat', 'phasederiv'] instead of {}".format(rep))
  

    def _load_wav(self, path):
        data, samplerate = sf.read(path)
        return data
        
#     def _compute_batch_ids(self, df, batch_size, seed=10):
#         # Old nonstratified batch creation
#         # Shuffle
#         df = df.sample(frac=1.0, random_state=seed)
#         # Batch_id mask
#         batch_ids = np.concatenate([[i] * batch_size for i in range(self.__len__())])[:self.total_samples]
#         df['batch_id'] = batch_ids
#         return df
    
    def _compute_batch_ids(self, df, batch_size, class_col, seed=10):
        # Produces stratified batches if it is possible. If classes are unequal, 
        # the last batches are the one affected.
        
        # Shuffle
        df = df.sample(frac=1.0, random_state=seed)
        
        # Group by class
        g = df.groupby(class_col)
        
        # Reset index of each group and concat together with sorted index
        # Results in alternating ordering or rows, e.g. A, B, C, A, B, C ...
        df = pd.concat([df.loc[index].reset_index(drop=True) for index in g.groups.values()]).sort_index()
        
        # Batch_id mask
        batch_ids = np.concatenate([[i] * batch_size for i in range(self.__len__())])[:self.total_samples]
        
        # Assign batch_id to each row
        df['batch_id'] = batch_ids
        return df

    
    def _class_names_to_labels(self, class_names):
        return np.array([self.labels.loc[name].values for name in class_names])

    
    def __getitem__(self, idx):
        batch_fname = '{}.npz'.format(idx)
        batch_path = os.path.join(self.cache, batch_fname)
        
        # Load the batch from ram cache if it is already there
        if batch_fname in self.ramcache.keys():
            x, y = self.ramcache[batch_fname]
            return x, y
        
        # Load the batch from disk cache if it is already created
        elif batch_fname in self.cached_batches:
            st = time.time()
            batch = np.load(batch_path)
            x, y =  batch['x'], self._class_names_to_labels(batch['class_names'])
        
        # Create the batch if it is not cached
        else: 
            batch_df = self.df.loc[self.df['batch_id'] == idx]    
            x = np.array([self._transform(self._load_wav(r['segment_path']), self.rep, self.tf_params)
                          for _, r in batch_df.iterrows()])
            class_names = np.array([r[self.class_col] for _, r in batch_df.iterrows()])
            y = self._class_names_to_labels(class_names)
            
            # Save the batch to disk cache
            np.savez(batch_path, x=x, class_names=class_names)
            self.cached_batches.append(batch_fname)
                
        # Scaling function is applied after saving the batch to diskcache, so the data in batches
        # are saved in their original form    
        if self.scaling_func:
            x = self.scaling_func(x)
        
        if self.keep_in_ram:
            self.ramcache[batch_fname] = (x, y)
            
        return x, y 


def run_experiment(get_model, opt, i, trainseq, validseq, testseq, features, train_params, space_element, variant_params, variant_name, paths):
    """
    Runs one experiment with specified data and parameters. 
    Saves the epoch log, parameters and model for each epoch in a special folder.
    
    get_model: callable
        Function that returns compiled Keras model
    i: int
        Enumeration of the experiment
    trainseq, validseq, testseq: keras sequence
        Delivers appropriate data
    features: np.array
        Additional features of the output for variable loss
    train_params: dict
        Basic train_params that are going to be updated and saved to log.
    space_element: np.array vector
        Additional hyperparameters that are being searched in order ['nkernel', 'lr', 'decay', 'drop', 'alpha', 'kr']
        Train params is updated using these.
    variant_params: dict
        Parameters that overwrites updated train_params to obtain 3 variants (cat_crossentropy, var_loss and projected)
    variant_name: str
        Just for giving the experiment folder the right suffix according to variant that was used
    paths: dict
        Dictionary containing config of paths for the experiment
    """
    
    # Create a dedicated folder for this experiment
    experiment_path = os.path.join(paths['path_experiments'], 'exp-{:04d}-{}'.format(i, variant_name))
    ensure_folder(experiment_path)
    
    # Update train parameters with params from random search
    names=['nkernel', 'lr', 'decay', 'drop', 'alpha', 'kr', 'project_n']
    train_params.update({name: value for name, value in zip(names, space_element)})
    train_params.update(variant_params)
    print(train_params)
        
    # Set up callbacks
    callbacks = [#EvaluateOnSequence(trainseq, 'train'),
                 EvaluateOnSequence(testseq, 'test'),
                 CSVLogger(os.path.join(experiment_path, 'training.csv')),
                 ModelCheckpoint(os.path.join(experiment_path, '{epoch:04d}.model'), monitor='val_acc', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1),
                 #EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=0, mode='auto', baseline=None, restore_best_weights=False)
                ]
    
    # Build the model
    project_n = int(train_params['project_n']) if train_params['project_n'] else train_params['project_n']
    model = get_model(opt=opt, bn_in=True, lr=train_params['lr'], drop=train_params['drop'], 
                 kr=train_params['kr'], decay=train_params['decay'], nkernel=train_params['nkernel'], 
                 loss=custom_loss(features, project_n=project_n, alpha=train_params['alpha']))
    
    
    # Save training parameters to file
    params_path = os.path.join(experiment_path, 'train_params.json')
    with open(params_path, 'w') as outfile:
        json.dump(train_params, outfile)
        
    # Save model source code to a file
    source_path = os.path.join(experiment_path, 'model_source.py')
    source = inspect.getsource(get_model)
    with open(source_path, 'w') as outfile:
        print(source, file=outfile)  
    
    # Seed for shuffling the batches
    np.random.seed(train_params['seed'])
    
    # Fit the model
    model.fit_generator(trainseq, 
                        epochs=train_params['epochs'], 
                        verbose=train_params['verbose'], 
                        callbacks=callbacks, 
                        validation_data=validseq,
                        max_queue_size=train_params['max_queue_size'],
                        workers=train_params['workers'],
                        use_multiprocessing=train_params['use_multiprocessing'],
                        shuffle=train_params['shuffle'],
                        initial_epoch=train_params['initial_epoch'])
    

    
    
########################
# EVALUATION
########################
    
def best_epochs(path_to_exp, out_file, sort_by='val_acc'):
    """
    Loads logs from all experiments in specified folder.
    Finds the best epoch based on validation accuracy.
    Creates csv with important info about each experiment.
    Prints the best 10 experiments as a pandas table.
    """

    logs = {}
    results = []
    
    experiments = [x for x in os.listdir(path_to_exp) if os.path.isdir(os.path.join(path_to_exp, x))]
    for experiment in experiments:
        number, variant = experiment.split('-')[1:]

        # Get epoch logs
        log = pd.read_csv(os.path.join(path_to_exp, experiment, 'training.csv'), index_col='epoch')
        log.name = experiment # Identifier of each log in log.name
        # Get used parameters
        with open(os.path.join(path_to_exp, experiment, 'train_params.json'), 'r') as infile:
            tpj = json.load(infile)
        used_param = pd.DataFrame(tpj, index=[number])
        used_param['model_no'] = int(number)
        used_param['variant'] = variant
        best_epoch = log['val_acc'].idxmax()
        used_param['best_epoch'] = best_epoch
        try:
            used_param['train_acc'] = log.iloc[best_epoch]['train_acc']
        except:
            used_param['acc'] = log.iloc[best_epoch]['acc']
        used_param['val_acc'] = log.iloc[best_epoch]['val_acc']
        used_param['test_acc'] = log.iloc[best_epoch]['test_acc']     
        used_param['exp'] = experiment
        results.append(used_param)

        if number in logs.keys():
            logs[number][variant] = log
        else:
            logs[number] = {}
            logs[number][variant] = log

    results = pd.concat(results).sort_values(by='model_no')
    results.reset_index(drop=True)
    results = results.sort_values(by=sort_by, ascending=False)
    results.to_csv(out_file)
    print(results.head(10))
    return results
  