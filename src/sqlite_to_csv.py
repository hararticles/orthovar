import sqlite3
import pandas as pd
import os

def convert_to_csv(dbfilepath, exportpath):
     db = sqlite3.connect(dbfilepath)
     cursor = db.cursor()
     cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
     tables = cursor.fetchall()
     for table_name in tables:
         table_name = table_name[0]
         table = pd.read_sql_query("SELECT * from %s" % table_name, db)
         table.to_csv(os.path.join(exportpath, '{}.csv'.format(table_name)), index_label='index')

if __name__ == '__main__':
    print('Specify sqlite file path:')
    dbfilepath = input()
    print('Specify path for exported csvs (just Enter for here):')
    exportpath = input()
    convert_to_csv(dbfilepath, exportpath)
    print('DONE')

